from django.shortcuts import render
# from function1 import text_utils

def home(request):
    return render(request, 'landing.html')

def textutils(request):
    return render(request, 'textutils.html', {'title':'App'})

def documentation(request):
    return render(request, 'documentation.html', {'title':'Documentation'})


def analyze(request):
    userinput= request.POST.get('user_input', 'default')

    # Check checkbox values
    removepunc = request.POST.get('removepunc', 'off')
    fullcaps = request.POST.get('fullcaps', 'off')
    newlineremover = request.POST.get('newlineremover', 'off')
    extraspaceremover = request.POST.get('extraspaceremover', 'off')

    #calling util fuction
    analyzed_text=''
    analyzed_text1 = ''

    punctuations = '''!()-[]{;}:'",<>./?@#$%^&*_\~'''
    print(punctuations)
    
    if removepunc=='on':
        for char in userinput:
            if char not in punctuations:
                analyzed_text+=char
        analyzed_text=analyzed_text
        
    if fullcaps == 'on':
        if analyzed_text=='':
            analyzed_text=userinput.upper()
        else:
            analyzed_text=analyzed_text.upper()

    if extraspaceremover == 'on':
            if analyzed_text=='':
                for index, char in enumerate(userinput):
                    if not(userinput[index] == " " and userinput[index+1]==" "):
                        analyzed_text = analyzed_text + char
            else:
                analyzed_text1=''
                for index, char in enumerate(analyzed_text):
                    if not(analyzed_text[index] == " " and analyzed_text[index+1]==" "):
                        analyzed_text1 = analyzed_text1 + char

    if newlineremover == 'on':
        if analyzed_text=='':
            for char in userinput:
                if char != '\n' and char != '\r':
                    analyzed_text = analyzed_text + char
        else: 
            analyzed_text=''
            for char in analyzed_text:
                if char != '\n' and char != '\r':
                    analyzed_text1 = analyzed_text1 + char
            analyzed_text=analyzed_text1
            print(analyzed_text)
            

    if analyzed_text == '':
        analyzed_text='Something went wrong. Try Select an option'


    # analyzed_text= text_utils(userinput)

    return render (request, 'analyze.html',{'title':'Analyzed Output', 'analyzed':analyzed_text})